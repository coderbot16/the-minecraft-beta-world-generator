# Common Algorithms

The implementations of the Perlin and Simplex noise generators do share some common initialization code. Using a Java Random number generator, they generate a random offset that is applied to input coordinates, as well as the permuations array used in both Simplex and Perlin noise generation.

### Random offset generation
First, the code generates a random 3D offset where the offset on each axis is within the range `[-256, 256]`. This is done by the expression `nextDouble() * 256`, which sets the value for the X axis, Y axis, and then Z axis.

### Generation of the Permutations array
Second, Notch replaces the existing static permutations array with his own separate array, generated with the following algorithm:

 1. Create a new byte array that is 512 bytes in size.
 2. Fill the first 256 entries with the respective index, so that `permutations[index] == index` where index is an integer between 0 and 255.
 3. For each index in 0 to 255 (the first 256 entries):
    1. Generate a second index in the range `[index, 256)` with the expression `nextInt(256 - index) + index`.
    2. Swap the integers referenced by each index.
 4. Copy the first 256 entries in the array to the last 256 entries, so that the array contains 2 sequential copies of the permuatations table.
