# Tree Generation

Minecraft generates trees with a relatively simple algorithm. Trees of this style have existed since the Classic era of the game, the only extension being the addition of Birch trees. Vanilla uses 2 separate classes for generating oak and birch, but there are only 2 minor differences between them. This style of copy/pasting while only modifying small bits is consistent across the level generation code.

In the context of setting blocks, `log`/`leaves` refers to the respective type: Oak or Birch. In the context of getting blocks, `leaves`, `grass`, and `dirt` only compare the block IDs and ignore the meta value.

## Select height

The very first step is to use the random number generator to pick a height.

* Oak Trees: Use the distribution `Linear { min: 4, max: 6 }`, or `4 + Random.nextInt(3)`.
* Birch Trees: Use the distribution `Linear { min: 5, max: 7 }`, or `5 + Random.nextInt(3)`.

## Checking the location

Make sure that there is space to grow a proper tree.

### Basic checks

1. Return if the origin Y is less than or equal to 0. This is because there is a check for a soil block below the trunk later in the code. If y is 0, there cannot possibly be a soil block below.
2. Return if `origin Y + height + 1` added together is greater than 127. This prevents attempts at placing trees over the build height.
3. Return if the block at `(X, Y, Z)` is not leaves or air.

Note: Notch performs part of the Y check in the bounding volume loop, but in this documentation I merged the two parts for simplicity.

### Bounding volume checks

For each Y offset in the range 1 (inclusive) to height + 1 (inclusive):

1. If the y offset is greater than or equal to `height - 1`, set the bounding radius to 2. Else, set the bounding radius to 1. 
2. For each block in bounds of `(X-BoundingRadius,Y,Z-BoundingRadius)` (inclusive) to `(X+BoundingRadius,Y,Z+BoundingRadius)` (inclusive), check if the block is leaves or air. If not, then return.

### Final checks 

Return if the block below is not grass or dirt. Notch also checks the origin Y value using another method as well, but as it turns out, his alternative method simplifies down to the combination of the 2 other checks.

## Placing the tree

### Soil

Set the block at `(X, Y-1, Z)` to dirt so that grass doesn't have to decay.

### Foliage

Place the 4 levels of foliage, with each having a level that is between 0 (inclusive) and 3 (inclusive). The foliage starts at 3 blocks below the height, so set the foliage offset to `height-3`. For each foliage level:

1. Set the y offset to the foliage offset added to the foliage level.
2. If the foliage level is 0 or 1, set the radius to 2. Otherwise, set the radius to 1.
3. For each block in bounds of `(X-Radius,Y,Z-Radius)` (inclusive) to `(X+Radius,Y,Z+Radius)` (inclusive):
	1. If the block is at the corner of the selection (X+/-Radius, Z+/-Radius), perform the following check before proceeding:
		1. Get a random integer that is either 0 or 1, using the call `Random.nextInt(2)`.
		2. If the random integer is 1 and the foliage level is not the top (3), then the check passes.
	2. If this block is a corner block and the preceding check fails, then skip this block.
	3. If the block is an opaque cube, then skip this block.
	4. Set the block to leaves.

### Trunk

For each Y value in the range `origin y` (inclusive) to `origin y + height` (exclusive), place a log block.

## Done!

Return true to indicate success.