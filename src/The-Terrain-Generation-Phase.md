# The Terrain Generation Phase

The Terrain Generation phase is perhaps the most formative of the generation phases. It is responsible for the majority of the basic terrain generation, and is divided into 4 major phases:

 1. Shape - Samples the output of the noise generation algorithms, forming the world into stone and air.
 2. Paint - Paints a biome-dependent surface on to the terrain, such as dirt, grass, sand, and gravel.
 3. Flood - Places water and ice below the sea level, to form oceans and lakes in the terrain.
 4. Carve - Carves caves into the terrain in a deterministic fashion. In addition, this places the lava at the very botton of the map - which is really just the lower level of caves.

While the Notchian generator merges step #3 into steps #1 and #2, it is generally more simple to treat it as a separate step for the purposes of this documentation.
