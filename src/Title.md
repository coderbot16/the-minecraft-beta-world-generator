# The Minecraft Beta World Generator

*by coderbot16*

Welcome to The Minecraft Beta World Generator book, providing unofficial documentation of the world generator from the beta versions of Mojang's extensively popular game, Minecraft! The goal of this book is to both provide insight into the internals of the algorithms used within the generator, and provide sufficient documentation to allow one to implement a generator that can generate identical terrain in a clean-room fashion.

While the primary goal of this documentation is to permit an exact recreation of the world generator seen in Minecraft Beta 1.7.3, an additional goal is to provide additional documentation that will aid in recreation of previous generator versions. Once this process is complete, a name change might be in order, yet for now the current name will suffice.

In addition, test files containing reference output from Notchian implementations of the world generator will be provided for the benefit of those wishing to implement their own copy of the world generator.
