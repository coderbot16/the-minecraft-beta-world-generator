Minecraft uses 3 variants of noise generator: Perlin3D, Perlin2D, and Simplex2D.

The MC implementation (which will be referred to as MCSimplex) does deviate from standard implementations in some ways. For reference, we will be comparing with the Java implementation at http://webstaff.itn.liu.se/~stegu/simplexnoise/.

An MCSimplex generator has