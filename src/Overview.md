# Overview

The world generation process can generally be divided into two major stages: Terrain Generation and Population. Terrain Generation creates a 16x128x16 volume of blocks from nothing, while population places various decorations (such as trees) within an already generated 32x128x32 volume of blocks. This divide is necessary to facilitate the incrementally generated infinite world of Minecraft.

## Terrain Generation

The first stage of the generation, this stage is responsible for creating the basic shape of the terrain. While the terrain is extremely barren at the end of this stage, it does not rely on the existence of any other chunk columns. There are several sub-stages to this process, including shaping, painting, flooding, and carving. These are explained in more detail in the [relevant section][Terrain Generation].

## Population

The second stage of the generation, this stage is responsible for placing various features on the terrain, including ores, trees, grass, flowers, and more. Much of the appearance of the terrain derives from this stage. More detail exists in the [relevant section][Population].

[Terrain Generation]: ./The-Terrain-Generation-Phase.md
[Population]: ./The-Population-Phase.md
