# The Population Phase

In this stage, the generator temporarily merges 4 different adjacent chunks into a single 32x128x32 volume in order to place features, such as trees, that can cross chunk boundaries. The generator has an ordered list of feature types that it generates sequentially, beginning at random water lakes and ending at placing snow.

Population deals with a chunk as well as its neighbors on +X, +Z, and +XZ. While all 4 chunks share in the results of the population phase, with the results being centered on the center between the 4 chunks, the root chunk is the one which has the honor of holding the `TerrainPopulated` flag.

### Pitfalls of the Population Process

An important aspect of the population phase is that it is not entirely deterministic. Since the population phase depends on the status of neighoring chunks, which may or may not have been populated themselves, population as a whole depends on the order of chunk population, which itself depends on one's exploration patterns. This issue [can be seen in modern Minecraft][Population Inconsistency], and reveals a common theme: modern versions of the game share much of the algorithms utilized in the initial revisions of the generation.

In sum, one trying to reimplement a world generator must consider this fact when verifying the results of their generation. If the vanilla generator fails to replicate its own population phase in many cases, it is hopeless to attempt to replicate it in a third-party implementation.

### An Interlude: Cascading World Generation

The Vanilla world generator provides populators simply with the entire world and a position, and trusts that they will not escape their 32x128x32 volume in the process of generation. However, this opens up the possibility of badly implemented populators that touch blocks outside of the specified volume. While this would normally not be an issue, the vanilla server by default will generate or load chunks on block get or set operations, meaning that such a badly implemented generator could lead to the generation of another chunk, and then the population of another chunk. The most severe instances of this can be seen in modifications of modern Minecraft, which combine many different and potentially flawed populator implementations which lead to significant performance issues, as documented in [a famous reddit post][Cascading World Generation].

When implementing a world generator, one must consider whether such a potential flaw is acceptable. `i73` enforces the volume in the most Rust-ic way possible: by only passing the relevant chunks to the generation process through a specialized type. Besides enabling parallelism, it also precludes this class of bug.

[Population Inconsistency]: https://bugs.mojang.com/browse/MC-55596
[Cascading World Generation]: https://www.reddit.com/r/feedthebeast/comments/5x0twz/investigating_extreme_worldgen_lag/
